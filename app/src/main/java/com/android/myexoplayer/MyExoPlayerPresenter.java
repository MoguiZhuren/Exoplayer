package com.android.myexoplayer;

import android.Manifest;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.WindowManager;


public class MyExoPlayerPresenter {
    public View decorView;
    /**
     * 控制状态栏
     */
    public void controlDecorView(final MyExoPlayerFragment fragment) {
        if (fragment == null || fragment.exoHandler == null) return;
        decorView = fragment.getActivity().getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WindowManager.LayoutParams localLayoutParams = fragment.getActivity().getWindow().getAttributes();
            localLayoutParams.flags = (WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | localLayoutParams.flags);
        }
        if (decorView == null) return;
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if (visibility == View.VISIBLE && fragment.getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                    fragment.exoHandler.sendEmptyMessageDelayed(fragment.DECOR_VIEW, 3000);
                }
            }
        });
    }

    /**
     * 获取权限
     *
     * @param activity
     */
    public void verifyStoragePermissions(Activity activity) {
        if (activity == null) return;
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            String[] PERMISSIONS_STORAGE = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
            int REQUEST_EXTERNAL_STORAGE = 1;
            ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
        }
    }

    /**
     * 屏幕切换
     * 1. View.SYSTEM_UI_FLAG_VISIBLE：显示状态栏，Activity不全屏显示(恢复到有状态的正常情况)。
     * 2. View.INVISIBLE：隐藏状态栏，同时Activity会伸展全屏显示。
     * 3. View.SYSTEM_UI_FLAG_FULLSCREEN：Activity全屏显示，且状态栏被隐藏覆盖掉。
     * 4. View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN：Activity全屏显示，但状态栏不会被隐藏覆盖，状态栏依然可见，Activity顶端布局部分会被状态遮住。
     * 5. View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION：效果同View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
     * 6. View.SYSTEM_UI_LAYOUT_FLAGS：效果同View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
     * 7. View.SYSTEM_UI_FLAG_HIDE_NAVIGATION：隐藏虚拟按键(导航栏)。有些手机会用虚拟按键来代替物理按键。
     * 8. View.SYSTEM_UI_FLAG_LOW_PROFILE：状态栏显示处于低能显示状态(low profile模式)，状态栏上一些图标显示会被隐藏。
     */
    public void configurationChanged(MyExoPlayerFragment fragment) {
        if (fragment == null) return;
        int mCurrentOrientation = fragment.getActivity().getResources().getConfiguration().orientation;
        if (fragment.fullscreen != null) {
            if (mCurrentOrientation == Configuration.ORIENTATION_PORTRAIT) {  //竖屏
                fragment.fullscreen.setImageResource(R.mipmap.player_landscape);
                if (fragment.exoHandler.hasMessages(fragment.DECOR_VIEW)) {
                    fragment.exoHandler.removeMessages(fragment.DECOR_VIEW);
                }
                if(decorView!=null){
                    decorView.setSystemUiVisibility(View.VISIBLE);
                }
            } else if (mCurrentOrientation == Configuration.ORIENTATION_LANDSCAPE) { //横屏
                fragment.fullscreen.setImageResource(R.mipmap.player_portrait);
                if (fragment.exoHandler.hasMessages(fragment.DECOR_VIEW)) {
                    fragment.exoHandler.removeMessages(fragment.DECOR_VIEW);
                }
                fragment.exoHandler.sendEmptyMessageDelayed(fragment.DECOR_VIEW, 3000);
            }
        }
    }

    /**
     * 注册Home键的监听
     */
    public void registerHomeListener(final MyExoPlayerFragment fragment) {
        if (fragment == null) return;
        fragment.mHomeWatcher = new HomeListener(fragment.getActivity());
        fragment.mHomeWatcher.setOnHomePressedListener(new HomeListener.OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                //TODO 进行点击Home键的处理
                fragment.releasePlayer();
            }

            @Override
            public void onHomeLongPressed() {
                //TODO 进行长按Home键的处理
            }
        });
        fragment.mHomeWatcher.startWatch();
    }


}
